import geometryToFlatEdges from './geometryToFlatEdges';

export default class SnapTarget {
    constructor(mesh, normal) {
        this.mesh = mesh;
        this.ownEdges = geometryToFlatEdges(mesh.geometry, normal);
        this._edges = null;
    }
    get edges() {
        if (!this._edges) {
            this._edges = this.ownEdges.map(e => e.clone().applyMatrix4(this.mesh.matrixWorld));
        } else {
            this.ownEdges.forEach((e, i) => {
                this._edges[i].copy(e).applyMatrix4(this.mesh.matrixWorld)
            });
        }
        return this._edges;
    }
}
