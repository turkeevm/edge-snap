import * as T from 'three';

const _geometryToEdges = geom => {
    if (geom instanceof T.Geometry) {
        return geometryToEdges(geom);
    }
    if (geom instanceof T.BufferGeometry) {
        return bufferGeometryToEdges(geom);
    }
    throw new Error('first argument should be an instance of THREE.Geometry or THREE.BufferGeometry');
};

const geometryToEdges = geom => {
    throw new Error('not implemented');
};


const bufferGeometryToEdges = geom => {
    const position = geom.attributes.position.array;
    const index = geom.index.array;

    const acc = [];
    for (let i = 0; i < index.length;) {
        const A = new T.Vector3().fromArray(position, index[i++] * 3);
        const B = new T.Vector3().fromArray(position, index[i++] * 3);
        const C = new T.Vector3().fromArray(position, index[i++] * 3);

        acc.push(
            new T.Line3(A, B),
            new T.Line3(B, C),
            new T.Line3(C, A)
        );
    }
    return acc;
};

const projectEdges = (edges, normal) => {
    const plane = new T.Plane(normal, 0);
    edges.forEach(edge => {
        edge.start = plane.projectPoint(edge.start);
        edge.end = plane.projectPoint(edge.end);
    });
    return edges;
};

const isCCW = (edge, center, normal) => {
    const cs = new T.Vector3().subVectors(edge.start, center);
    const delta = edge.delta();
    const direction = new T.Vector3().crossVectors(normal, cs);
    return direction.dot(delta) > 0;
};

const nKey = n => Math.round(n * 1e6);
const vKey = v => `${nKey(v.x)}:${nKey(v.y)}:${nKey(v.z)}`;
const eKey = e => `${vKey(e.start)}:${vKey(e.end)}`;

const deduplicateEdges = (edges) => {
    const acc = {};
    for (let i = 0; i < edges.length; i++) {
        const e = edges[i];
        const k = eKey(e);
        acc[k] = e;
    }
    return Object.values(acc);
};

export default (geom, normal) => {
    const edges = deduplicateEdges(projectEdges(_geometryToEdges(geom), normal));

    const center = edges
        .reduce((acc, e) => acc.add(e.start), new T.Vector3())
        .multiplyScalar(1 / edges.length);


    return edges.filter(e => isCCW(e, center, normal));
}
