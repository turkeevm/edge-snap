import * as T from 'three';

export const makeSnapParameters = do {
    const oDir = new T.Vector3();
    const tDir = new T.Vector3();
    (objectEdge, toEdge) => {
        const oCenter = objectEdge.at(.5);

        const delta = Math.sqrt(objectEdge.distanceSq() / toEdge.distanceSq()) / 2;
        const projectedOCenterParameter = T.Math.clamp(
            toEdge.closestPointToPointParameter(oCenter),
            -delta,
            1 + delta
        );

        const snappedOCenter = toEdge.at(projectedOCenterParameter);

        oDir.subVectors(objectEdge.end, objectEdge.start).normalize();
        tDir.subVectors(toEdge.start, toEdge.end).normalize();

        const quaternion = new T.Quaternion().setFromUnitVectors(oDir, tDir);

        return {
            distanceSq: oCenter.distanceToSquared(snappedOCenter),
            quaternion,
            oCenter,
            snappedOCenter,
        };
    };
};

export const makeTransformFromParameters = do {
    const translationBack = new T.Matrix4();
    const rotation = new T.Matrix4();
    const translationForth = new T.Matrix4();
    const negativeOCenter = new T.Vector3();
    (snapParameters) => {
        negativeOCenter.copy(snapParameters.oCenter).negate();
        translationBack.setPosition(negativeOCenter);
        rotation.makeRotationFromQuaternion(snapParameters.quaternion);
        translationForth.setPosition(snapParameters.snappedOCenter);

        return translationBack
            .clone()
            .premultiply(rotation)
            .premultiply(translationForth);
    };
};

export const makeTransformFromEdges = (e1, e2) => makeTransformFromParameters(makeSnapParameters(e1, e2));

const bestSnapParameters2 = (a, b) => {
    if (a.distanceSq < b.distanceSq) return a;
    if (a.distanceSq > b.distanceSq) return b;
    if (a.quaternion.w > b.quaternion.w) return a;
    return b;
};

export const findBestSnap = (snapParameterList, maxDist, maxAngle) => {
    const maxDistSq = maxDist ** 2;
    const cosMaxAngleOver2 = Math.cos(maxAngle / 2);
    const filtered = snapParameterList
        .filter(({distanceSq, quaternion}) => quaternion.w > cosMaxAngleOver2 && distanceSq < maxDistSq);
    if (filtered.length === 0) {
        return null;
    }
    return filtered.reduce(bestSnapParameters2);
};
