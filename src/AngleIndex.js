import * as T from 'three';

export default class AngleIndex {
    constructor(normal) {
        this.normal = normal;
        this.all = [];
    }
    clear() {
        this.all = [];
    }
    add(edge, snapTarget) {
        const edgeNormal = new T.Vector3().crossVectors(edge.delta(), this.normal);
        const angle = Math.atan2(edgeNormal.y, edgeNormal.x);
        this.all.push({
            angle,
            snapTarget
        });
    }
    testEdge(edge, thisItem, maxAngle) {
        const edgeNormal = new T.Vector3().crossVectors(edge.delta(), this.normal);
        const angle = Math.atan2(edgeNormal.y, edgeNormal.x);
        return Math.abs(angle - thisItem.angle) < maxAngle
            || Math.abs(angle - (thisItem.angle - Math.PI * 2)) < maxAngle
            || Math.abs(angle - (thisItem.angle + Math.PI * 2)) < maxAngle;
    }
}
