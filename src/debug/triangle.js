import * as T from 'three';

const makeTriangle = (t, color=0xff0000) => {
    const geometry = new T.Geometry();
    geometry.vertices.push(t.a, t.b, t.c);
    geometry.faces.push(new T.Face3(0, 1, 2, t.normal(), color));

    return new T.Mesh(
        geometry,
        new T.MeshBasicMaterial({color, side: T.DoubleSide})
    );
};

export default makeTriangle;
