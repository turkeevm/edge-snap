import QueryResult from './QueryResult';
import {pred, succ} from './binSearch';

export default class AxisIndex {
    constructor(axis) {
        this.axis = axis;
        this.all = [];
        this.sorted = false;
    }
    add(edge, snapTarget) {
        this.all.push({
            type: 'start',
            point: this.axis.closestPointToPointParameter(edge.start),
            edge,
            snapTarget
        }, {
            type: 'end',
            point: this.axis.closestPointToPointParameter(edge.end),
            edge,
            snapTarget
        });
    }
    sort() {
        this.all.sort((a, b) => a.point - b.point);
        this.sorted = true;
    }
    query(edge, maxDistance) {
        if (!this.sorted) {
            throw new Error(`index should be sorted before querying`);
        }
        const s = this.axis.closestPointToPointParameter(edge.start);
        const e = this.axis.closestPointToPointParameter(edge.end);
        const min = Math.min(s, e) - maxDistance;
        const max = Math.max(s, e) + maxDistance;

        const minI = Math.max(
            0,
            pred(this.all, min, r => r.point)
        );
        const maxI = Math.min(
            this.all.length - 1,
            succ(this.all, max, r => r.point)
        );

        return new QueryResult(this, minI, maxI - minI);
    }
}
