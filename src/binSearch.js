export const pred = (xs, x, f) => {
    const predR = (l, r, xl, xr) => {
        if (l === r) {
            if (x <= xl) return l - 1;
            return l;
        }
        if (l + 1 === r) {
            if (x <= xl) return l - 1;
            if (x <= xr) return l;
            return r;
        }
        const m = (l + r) >> 1;
        const xm = f(xs[m]);
        if (x < xm) return predR(l, m, xl, xm);
        if (x > xm) return predR(m, r, xm, xr);
        return m - 1;
    };

    return predR(0, xs.length - 1, f(xs[0]), f(xs[xs.length - 1]));
};

export const succ = (xs, x, f) => {
    const succR = (l, r, xl, xr) => {
        if (l === r) {
            if (x >= xr) return r + 1;
            return r;
        }
        if (l + 1 === r) {
            if (x >= xr) return r + 1;
            if (x >= xl) return r;
            return l;
        }
        const m = (l + r) >> 1;
        const xm = f(xs[m]);
        if (x < xm) return succR(l, m, xl, xm);
        if (x > xm) return succR(m, r, xm, xr);
        return m + 1;
    };

    return succR(0, xs.length - 1, f(xs[0]), f(xs[xs.length - 1]));
};
