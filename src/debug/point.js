import * as THREE from 'three';
import Pool from './pool';

const makePoint = (pos, color = 0xff0000) => {
    const point = new THREE.Mesh(
        new THREE.SphereGeometry(.02),
        new THREE.MeshBasicMaterial({
            depthTest: false,
            color,
            opacity: .5,
            transparent: true
        })
    );
    point.userData.isPoint = true;

    point.position.copy(pos);

    return point;
};

const p = new Pool({
    create: makePoint,
    reuse(point, pos, color = 0x00ff00) {
        point.material.color.setHex(color);
        point.position.copy(pos);
    }
});

export default (pos, color = 0x00ff00) => p.get(pos, color);
