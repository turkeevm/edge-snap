import * as T from 'three';
import 'three/examples/js/controls/OrbitControls';
import 'three/examples/js/controls/TransformControls';
import makeDbg from './debug';

import SnapTarget from './SnapTarget';

import {findBestSnap, makeSnapParameters, makeTransformFromParameters} from './snap';
import AxisIndex from "./AxisIndex";
import AngleIndex from "./AngleIndex";

// import makeGui from 'b3-gui-2';

const scene = new T.Scene();
const renderer = new T.WebGLRenderer({
    alpha: true
});
const camera = new T.PerspectiveCamera();
const controls = new T.OrbitControls(camera, renderer.domElement);
const directionalLight = new T.DirectionalLight();
const ambientLight = new T.AmbientLight();

camera.position.set(-2, 2, 10);
directionalLight.position.set(5, 2, 0);

const zAxis = new T.Vector3(0, 0, 1);

const makeCube = (pos, color, update, size = 1) => {
    const m = new T.Mesh(
        new T.BoxBufferGeometry(.7, .7, .7),
        new T.MeshBasicMaterial({
            color,
            wireframe: true
        })
    );
    const white = new T.Mesh(m.geometry, new T.MeshBasicMaterial({color: 0xffffff}));
    m.material.transparent = white.material.transparent = true;
    m.material.opacity = white.material.opacity = .9;

    white.scale.set(.99, .99, .99);
    m.add(white);
    m.position.copy(pos);
    m.scale.multiplyScalar(size);

    m.userData = m.userData || {};
    m.userData.skip = false;

    return {
        object: m,
        update,
        snapTarget: new SnapTarget(m, zAxis)
    };
};

const rotation = speed => (t, o) => {
    o.rotation.z = (t / 1000) * (speed * Math.PI * 2);
    // o.rotation.z = speed * Math.PI * 2;
};

const c0 = makeCube(new T.Vector3(-.5, .7, 0), 0xff0000, (t, o) => {});

const n = 100;
const s = Math.sqrt(n) * 3;
const rndCoord = () => (Math.random() - .5) * s;
const rndRecords = new Array(n).fill(0).map(() => makeCube(new T.Vector3(rndCoord(), rndCoord(), 0), 0x000000, rotation(Math.random()), 2));

const records = rndRecords;
/*
const records = [
    makeCube(new T.Vector3(-1.1, -1.2, 0), 0x000000, rotation(0.031)),
    makeCube(new T.Vector3(-1.3, 1.4, 0), 0x000000, rotation(0.032)),
    makeCube(new T.Vector3(1.5, -1.6, 0), 0x000000, rotation(0.036)),
    makeCube(new T.Vector3(1.7, 1.8, 0), 0x000000, rotation(0.039)),
    makeCube(new T.Vector3(-1.8, 0, 0), 0x000000, rotation(0.04)),
    makeCube(new T.Vector3(0, 1.9, 0), 0x000000, rotation(0.041)),
    makeCube(new T.Vector3(0, -2, 0), 0x000000, rotation(0.042)),
    makeCube(new T.Vector3(2.1, 0, 0), 0x000000, rotation(0.043)),
    makeCube(new T.Vector3(-2.2, -1.2, 0), 0x000000, rotation(0.044)),
    makeCube(new T.Vector3(-1.3, 2.3, 0), 0x000000, rotation(0.045)),
    makeCube(new T.Vector3(2.6, -1.3, 0), 0x000000, rotation(0.046)),
    makeCube(new T.Vector3(1.7, 2.7, 0), 0x000000, rotation(0.047)),
    makeCube(new T.Vector3(-2.7, 0, 0), 0x000000, rotation(0.48)),
    makeCube(new T.Vector3(0, 2.8, 0), 0x000000, rotation(0.049)),
    makeCube(new T.Vector3(1, -.9, 0), 0x000000, rotation(0.044)),
    makeCube(new T.Vector3(.3, -.4, 0), 0x000000, rotation(0.051))
];

*/
const updateObjects = t => records.forEach(r => {
    if (!r.object.userData.skip) {
        r.update(t, r.object);
        r.object.updateMatrixWorld();
    }
    r.object.userData.skip = false;
}) ;

const dbg = makeDbg(scene);

const tr = new T.TransformControls(camera, renderer.domElement);

scene.add(
    c0.object, tr,
    ...records.map(r => r.object),
    directionalLight,
    ambientLight,
    new T.AxisHelper(100)
);
tr.attach(c0.object);

const raf = () => new Promise(resolve => requestAnimationFrame(resolve));

/*
const gui = makeGui(window.gui, {
    renderer,
    scene,
    camera,
    controls
});
*/

while (c0.object.children.length) {
    c0.object.remove(c0.object.children[0]);
}

const cMat = color => {
    const mat = new T.MeshBasicMaterial({color});
    mat.polygonOffset = true;
    mat.polygonOffsetFactor = 1;
    mat.polygonOffsetUnits = 1;
    return mat;
};

c0.object.material = [
    cMat(0xff0000),
    cMat(0x00ff00),
    cMat(0x0000ff),
    cMat(0x00ffff),
    cMat(0xff00ff),
    cMat(0xffff00)
];

const edgeColors = [
    [0xff0000, new T.Vector3(0, 1, 0)],
    [0x00ff00, new T.Vector3(0, -1, 0)],
    [0x0000ff, new T.Vector3(-1, 0, 0)],
    [0x00ffff, new T.Vector3(1, 0, 0)]
];

const getEdgeColor = edge => edgeColors.find(
    e => (Math.abs(1 - edge.delta().normalize().dot(e[1])) < 1e-6)
)[0];

const snapped = c0.object.clone();
snapped.matrixAutoUpdate = false;
snapped.material = snapped.material.map(_m => {
    const m = _m.clone();
    m.transparent = true;
    m.opacity = .5;
    return m;
});
scene.add(snapped);

const xAxisIndex = new AxisIndex(new T.Line3(new T.Vector3(0, -2, 0), new T.Vector3(1, -2, 0)));
const yAxisIndex = new AxisIndex(new T.Line3(new T.Vector3(-2, 0, 0), new T.Vector3(-2, 1, 0)));
const aIndex = new AngleIndex(zAxis);

const MAX_SNAP_DISTANCE = .5;
const MAX_SNAP_ANGLE = Math.PI / 6;

let time = 0;
const timeout = t => new Promise(resolve => setTimeout(() => resolve(time += t), t));

const animate = async () => {
    updateObjects(10000);
    records.forEach(r => {
        r.snapTarget.edges.forEach(edge => {
            xAxisIndex.add(edge, r.snapTarget);
            yAxisIndex.add(edge, r.snapTarget);
        });
    });
    xAxisIndex.sort();
    yAxisIndex.sort();

    while (true) {
        // await timeout(1000);
        await raf();
        dbg.clear();

        // gui.animate();

        console.time('scanForBestSnap');
        const snaps = [];

        c0.snapTarget.edges.forEach(objectEdge => {
            const xQuery = xAxisIndex.query(objectEdge, MAX_SNAP_DISTANCE);
            const yQuery = yAxisIndex.query(objectEdge, MAX_SNAP_DISTANCE);
            const bestQuery = xQuery.count < yQuery.count ? xQuery : yQuery;
            // console.log(bestQuery.count);
            bestQuery.forEach(r => {
                snaps.push(makeSnapParameters(objectEdge, r.edge));
                // dbg.edge(r.edge);
            });
        });

        const parameters = findBestSnap(snaps, MAX_SNAP_DISTANCE, MAX_SNAP_ANGLE);

        snapped.visible = false;
        if (parameters) {
            const transform = makeTransformFromParameters(parameters);
            snapped.matrix.copy(c0.object.matrix);
            snapped.matrix.premultiply(transform);
            snapped.updateMatrixWorld();
            snapped.visible = true;
        }
        console.timeEnd('scanForBestSnap');

/*
        c0.snapTarget.edges.forEach(e => dbg.edge(e, 0xffffff));
        records.forEach(r => r.snapTarget.edges.forEach((e, i) => dbg.edge(e, getEdgeColor(r.snapTarget.ownEdges[i]))));
*/

        tr.update();
        controls.update();
        renderer.render(scene, camera);
    }
};

const resize = () => {
    const w = renderer.domElement.offsetWidth;
    const h = renderer.domElement.offsetHeight;

    renderer.setSize(w, h);
    camera.aspect = w / h;
    camera.updateProjectionMatrix();
};

animate()
    .then(
        () => {
            console.log('exit animation loop');
        },
        e => {
            console.error(e);
        }
    );

document.body.appendChild(renderer.domElement);

resize();
window.addEventListener('resize', resize);

const modes = ["translate", "rotate", "scale"];
let iMode = 0;
window.addEventListener("keydown", e => {
    if (e.keyCode === 32) {
        iMode = (iMode + 1) % 3;
        tr.setMode(modes[iMode]);
    }
});
