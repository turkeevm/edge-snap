import makeArrow from './arrow';
import makePoint from './point';
import makeTriangle from './triangle';

export {makeArrow, makeTriangle};

export default root => {
    let log = [];
    const arrow = (origin, dir, color = 0xff0000) => {
        const item = makeArrow(origin, dir, color);
        log.push(item);
        root.add(item);
    };
    return {
        arrow,
        point(pos, color = 0xff0000) {
            const item = makePoint(pos, color);
            log.push(item);
            root.add(item);
        },
        clear() {
            log.forEach(item => {
                item.parent.remove(item);
                item.stash();
            });
            log = [];
        },
        triangle(t, color) {
            const item = makeTriangle(t, color);
            log.push(item);
            root.add(item);
        },
        edge(e, color = 0xff0000) {
            arrow(e.start, e.delta(), color);
        }
    };
}
