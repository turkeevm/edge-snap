export default class QueryResult {
    constructor(index, from, count) {
        this.index = index;
        this.from = from;
        this.count = count;
    }
    get(i) {
        return this.index.all[this.from + i];
    }
    map(f) {
        const from = this.from;
        const to = from + this.count;
        const acc = new Array(this.count);
        for (let i = from; i < to; i++) {
            acc[i] = f(this.index.all[i], i);
        }
        return acc;
    }
    forEach(f) {
        const from = this.from;
        const to = from + this.count;
        for (let i = from; i < to; i++) {
            f(this.index.all[i], i);
        }
    }
}
