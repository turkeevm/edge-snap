const path = require('path');
const findPort = require('find-port');

const p = (...args) => path.resolve(__dirname, ...args);

const host = '0.0.0.0';

const getPort = () => new Promise(resolve => findPort(host, 9000, 9010, ps => resolve(ps[0])));

module.exports = getPort().then(port => ({
    entry: [
        'regenerator-runtime/runtime',
        p('./src/index.js')
    ],
    output: {
        filename: 'j.js',
        path: p('./dist/'),
        publicPath: '/'
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            }, {
                test: /\.js$/,
                include: [
                    /three[\/\\]examples/
                ],
                loader: 'imports-loader?THREE=three'
            }, {
                test: /\.sass$/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            }, {
                test: /\.(png|jpg|svg)$/,
                use: ['file-loader']
            }, {
                test: /\.(vert|frag)$/,
                use: ['raw-loader']
            }
        ]
    },
    devServer: {
        contentBase: p('./public'),
        clientLogLevel: 'none',
        port,
        host
    }
}));
