export default class Pool {
    constructor({create, reuse}) {
        this.storage = new Set();
        this.create = create;
        this.reuse = reuse;
    }

    get(...args) {
        if (this.storage.size !== 0) {
            for (let obj of this.storage) {
                this.storage.delete(obj);
                this.reuse(obj, ...args);
                if (obj.userData.isPoint) {
                    // console.log(obj.material.id, this.storage.size);
                }
                return obj;
            }
        }
        const obj = this.create(...args);
        obj.stash = () => this.stash(obj);
        return obj;
    }

    stash(obj) {
        this.storage.add(obj);
    }
}
