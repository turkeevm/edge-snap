import {Color, ArrowHelper, Vector3} from 'three';
import Pool from './pool';

const makeArrow = (origin, dir, color = 0x00ff00) => {
    const arrow = new ArrowHelper(new Vector3(1, 0, 0), new Vector3(1, 0, 0), dir.length(), 0xff0000, 0.1, 0.05);
    arrow.userData.noRaycast = true;

    arrow.setDirection(dir.clone().normalize());
    arrow.setColor(new Color(color));
    arrow.line.material.linewidth = 5;
    arrow.line.material.polygonOffset = true;
    arrow.line.material.polygonOffsetFactor = -1;
    arrow.line.material.polygonOffsetUnits = -1;

    arrow.position.copy(origin);

    // arrow.scale.multiplyScalar(.1);

    return arrow;
};

const p = new Pool({
    create: makeArrow,
    reuse(arrow, origin, dir, color = 0x00ff00) {
        arrow.setDirection(dir.clone().normalize());
        arrow.setColor(new Color(color));
        arrow.position.copy(origin);
    }
});

export default (origin, dir, color = 0x00ff00) => p.get(origin, dir, color);
